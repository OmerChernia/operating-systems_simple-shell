# Simple Shell

This project implements a simple shell in C that allows users to execute commands in both foreground and background. The shell also maintains a history of executed commands.

## Features

- Execute commands in the foreground
- Execute commands in the background using `&`
- Exit the shell with the `exit` command
- View the history of commands with the `history` command

## Getting Started

### Prerequisites

- GCC compiler

### Building the Project

To compile the shell, use the following command:

gcc -Werror -std=c99 myshell.c -o myshell
